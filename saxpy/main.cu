#include <iostream>
#include <vector>
#include "common.hpp"
#include <random>
#include <cuda_runtime.h>

static double saxpy_cpu(std::vector<float>& y, const std::vector<float>& x, const std::vector<float>& b, float a, size_t num)
{

    double start = get_ms();

    for(size_t i = 0; i < num; ++i) {
	y[i] = a * x[i] + b[i];
    }

    double end = get_ms();

    return (end - start);
}

__global__ void
saxpy_device(float* y, float* x, float* b, float a, unsigned int num)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if(idx < num) {
	y[idx] = x[idx] * a + b[idx];
    }
}

static double saxpy_gpu(std::vector<float>& y, const std::vector<float>& x, const std::vector<float>& b, float a, size_t num)
{
    float* dy;
    float* dx;
    float* db;

    cudaError_t err;
    err = cudaMalloc((void**)&dy, sizeof(float) * num);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to allocate device vector y %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMalloc((void**)&dx, sizeof(float) * num);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to allocate device vector x %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMalloc((void**)&db, sizeof(float) * num);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to allocate device vector b %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMemcpy(dx, &x[0], sizeof(float) * num, cudaMemcpyHostToDevice);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to copy vector x %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMemcpy(db, &b[0], sizeof(float) * num, cudaMemcpyHostToDevice);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to copy vector x %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    int threadsPerBlock = 256;
    int blocksPerGrid = (num + threadsPerBlock - 1) / threadsPerBlock;

    unsigned int n = static_cast<unsigned int>(num);
    double start = get_ms();
    saxpy_device<<<blocksPerGrid, threadsPerBlock>>>(dy, dx, db, a, n);
    cudaDeviceSynchronize();
    double end = get_ms();

    cudaMemcpy(&y[0], dy, sizeof(float) * num, cudaMemcpyDeviceToHost);

    cudaFree(dy);
    cudaFree(dx);
    cudaFree(db);

    return (end - start);
}

template <typename T>
static bool verify(const std::vector<T>& l, const std::vector<T>& r, size_t num)
{
    bool isSame = true;
    for(size_t i = 0; i < num; ++i) {
	if(fabs(l[i] - r[i]) > 1.0e-6) {
	    std::cout << "failed @ " << i << " " << l[i] << " " << r[i] << std::endl;
	    isSame = false;
	    break;
	}
    }

    return isSame;
}

int main()
{
    const size_t num = 100000;

    std::vector<float> x(num);
    std::vector<float> cpu_y(num, 0.0f);
    std::vector<float> gpu_y(num, 0.0f);
    std::vector<float> b(num);

    std::random_device rnd;
    std::mt19937 mt(rnd());

    initVector(x, mt);
    initVector(b, mt);

    float a = 3.0f;

    const size_t loop_num = 10;

    double cpu_time = 0.0;
    saxpy_cpu(cpu_y, x, b, a, num);
    for(size_t i = 0; i < loop_num; ++i) {
	cpu_time += saxpy_cpu(cpu_y, x, b, a, num);
    }
    cpu_time /= static_cast<double>(loop_num);

    double gpu_time = 0.0;
    saxpy_gpu(gpu_y, x, b, a, num);
    for(size_t i = 0; i < loop_num; ++i) {
	gpu_time += saxpy_gpu(gpu_y, x, b, a, num);
    }
    gpu_time /= static_cast<double>(loop_num);

    if(verify(cpu_y, gpu_y, num)) {
	std::cout << "verify complete " << std::endl;
    }

    std::cout << "saxpy cpu : " << cpu_time << " msec." << std::endl;
    std::cout << "saxpy gpu : " << gpu_time << " msec." << std::endl;

    return 0;
}
