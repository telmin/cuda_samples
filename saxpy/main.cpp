#include <iostream>
#include <vector>
#include "common.hpp"
#include <random>

int main()
{
    const size_t num = 100000000;

    std::vector<float> x(num);
    std::vector<float> y(num, 0.0f);
    std::vector<float> b(num);

    std::random_device rnd;
    std::mt19937 mt(rnd());

    initVector(x, mt);
    initVector(b, mt);

    float a = 3.0f;

    double start = get_ms();

    for(size_t i = 0; i < num; ++i) {
	y[i] = a * x[i] + b[i];
    }

    double end = get_ms();

    for(size_t i = 0; i < 10; ++i) {
	std::cout << "y a x b " << y[i] << " " << a << " " << x[i] << " " << b[i] << std::endl;
    }

    std::cout << "saxpy : " << end - start << " msec." << std::endl;

    return 0;
}
