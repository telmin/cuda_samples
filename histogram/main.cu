#include <iostream>
#include <vector>
#include <sys/time.h>
#include <cstdlib>
#include <cassert>
#include "common.hpp"

static double calcHistogram_ref(const std::vector<unsigned char>& src, std::vector<int>& dst, const size_t width, const size_t height)
{
    double start = get_ms();

    for(size_t y = 0; y < height; ++y) {
	for(size_t x = 0; x < width; ++x) {
	    const size_t idx = src[y * width + x];
	    dst[idx]++;
	}
    }

    double end = get_ms();

    return end - start;
}

__global__ void
histogram_cuda_kernel(const unsigned char* src, int* dst, const unsigned int width, const unsigned int height, const unsigned int num_elements)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int x = idx % width;
    int y = idx / width;

    if(idx < num_elements) {
        unsigned char val = src[y * width + x];
        atomicAdd(&dst[val], 1);
    }
}

static double calcHistogram_cuda(const std::vector<unsigned char>& src, std::vector<int>& dst, const size_t width, const size_t height)
{
    cudaError_t err = cudaSuccess;

    unsigned char* d_src = NULL;
    err = cudaMalloc((void** )&d_src, sizeof(unsigned char) * width * height);
    assert(err == cudaSuccess);

    int* d_dst = NULL;
    err = cudaMalloc((void** )&d_dst, sizeof(int) * 256);
    assert(err == cudaSuccess);

    err = cudaMemcpy(d_src, &src[0], sizeof(unsigned char) * width * height, cudaMemcpyHostToDevice);
    assert(err == cudaSuccess);

    err = cudaMemcpy(d_dst, &dst[0], sizeof(int) * 256, cudaMemcpyHostToDevice);
    assert(err == cudaSuccess);

    unsigned int num_elements = width * height;

    int threads_per_block = 512;
    int blocks_per_grid = (num_elements + threads_per_block - 1 ) / threads_per_block;
//    std::cout << "CUDA Kernel launch with " << blocks_per_grid << " blocks of " << threads_per_block << " threads. " << std::endl;

    double start = get_ms();
    histogram_cuda_kernel<<<blocks_per_grid, threads_per_block>>>(d_src, d_dst, static_cast<unsigned int>(width), static_cast<unsigned int>(height), num_elements);
    cudaDeviceSynchronize();
    double end = get_ms();
    err = cudaGetLastError();
    assert(err == cudaSuccess);

    cudaMemcpy(&dst[0], d_dst, sizeof(int) * 256, cudaMemcpyDeviceToHost);
    assert(err == cudaSuccess);

    cudaFree(d_src);
    cudaFree(d_dst);

    return end - start;
}

__global__ void
histogram_cuda_shared_kernel(const unsigned char* src, int* dst, const unsigned int width, const unsigned int height, const unsigned int num_elements)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int x = idx % width;
    int y = idx / width;

    __shared__ int tmpHist[256];

    if(threadIdx.x < 256) {
	for(size_t i = 0; i < 256; i += blockDim.x) {
	    tmpHist[threadIdx.x + i] = 0;
	}
    }
    __syncthreads();

    if(idx < num_elements) {
        unsigned char val = src[y * width + x];
	atomicAdd(&tmpHist[val], 1);
    }
    __syncthreads();

    if(threadIdx.x < 256) {
	for(size_t i = 0; i < 256; i += blockDim.x) {
	    atomicAdd(&dst[threadIdx.x + i], tmpHist[threadIdx.x + i]);
	}
    }
}

static double calcHistogram_cuda_shared(const std::vector<unsigned char>& src, std::vector<int>& dst, const size_t width, const size_t height)
{
    cudaError_t err = cudaSuccess;

    unsigned char* d_src = NULL;
    err = cudaMalloc((void** )&d_src, sizeof(unsigned char) * width * height);
    assert(err == cudaSuccess);

    int* d_dst = NULL;
    err = cudaMalloc((void** )&d_dst, sizeof(int) * 256);
    assert(err == cudaSuccess);

    err = cudaMemcpy(d_src, &src[0], sizeof(unsigned char) * width * height, cudaMemcpyHostToDevice);
    assert(err == cudaSuccess);

    err = cudaMemcpy(d_dst, &dst[0], sizeof(int) * 256, cudaMemcpyHostToDevice);
    assert(err == cudaSuccess);

    unsigned int num_elements = width * height;

    int threads_per_block = 512;
    int blocks_per_grid = (num_elements + threads_per_block - 1 ) / threads_per_block;
//    std::cout << "CUDA Kernel launch with " << blocks_per_grid << " blocks of " << threads_per_block << " threads. " << std::endl;

    double start = get_ms();
    histogram_cuda_shared_kernel<<<blocks_per_grid, threads_per_block>>>(d_src, d_dst, static_cast<unsigned int>(width), static_cast<unsigned int>(height), num_elements);
    cudaDeviceSynchronize();
    double end = get_ms();
    err = cudaGetLastError();
    assert(err == cudaSuccess);

    cudaMemcpy(&dst[0], d_dst, sizeof(int) * 256, cudaMemcpyDeviceToHost);
    assert(err == cudaSuccess);

    cudaFree(d_src);
    cudaFree(d_dst);

    return end - start;
}

static int verify_histogram(const std::vector<int>& lhv, const std::vector<int>& rhv)
{
    const size_t num = lhv.size();

    for(size_t i = 0; i < num; ++i) {
        if(lhv[i] != rhv[i]) {
	    std::cout << "is not same at " << i << " " << lhv[i] << "," << rhv[i] << std::endl;
            return 0;
        }
    }

    return 1;
}

int main(int argc, char** argv)
{
    size_t width = 1920;
    size_t height = 1080;

    const size_t loop_count = 100;

    std::vector<unsigned char> src(width * height);
    std::vector<int> dst0(256, 0);
    std::vector<int> dst1(256, 0);
    std::vector<int> dst2(256, 0);

    // initialize
    for(size_t i = 0; i < width * height; ++i) {
        src[i] = static_cast<unsigned int>(rand() % 256);
    }

    double ref_time = 0.0;
    calcHistogram_ref(src, dst0, width, height);
    for(size_t i = 0; i < loop_count; ++i) {
	std::fill(dst0.begin(), dst0.end(), 0.0f);
        ref_time += calcHistogram_ref(src, dst0, width, height);
    }
    ref_time /= static_cast<double>(loop_count);
    double ref_pixel = ((width * height) / ref_time) / 1000000.0 * 1000;

    double cuda_time = 0.0;
    calcHistogram_cuda(src, dst1, width, height);
    for(size_t i = 0; i < loop_count; ++i) {
	std::fill(dst1.begin(), dst1.end(), 0.0f);
        cuda_time += calcHistogram_cuda(src, dst1, width, height);
    }
    cuda_time /= static_cast<double>(loop_count);
    double cuda_pixel = ((width * height) / cuda_time) / 1000000.0 * 1000;

    double cuda2_time = 0.0;
    calcHistogram_cuda_shared(src, dst2, width, height);
    for(size_t i = 0; i < loop_count; ++i) {
	std::fill(dst2.begin(), dst2.end(), 0.0f);
        cuda2_time += calcHistogram_cuda_shared(src, dst2, width, height);
    }
    cuda2_time /= static_cast<double>(loop_count);
    double cuda2_pixel = ((width * height) / cuda2_time) / 1000000.0 * 1000;

    // verify
    verify_histogram(dst0, dst1);
    verify_histogram(dst0, dst2);

    std::cout << "Histogram ref : " << ref_time << " msec. " << ref_pixel << " Milion Pixel/s" << std::endl;
    std::cout << "Histogram cuda : " << cuda_time << " msec. " << cuda_pixel << " Milion Pixel/s" << std::endl;
    std::cout << "Histogram cuda2 : " << cuda2_time << " msec. " << cuda2_pixel << " Milion Pixel/s" << std::endl;

    return 0;
}
