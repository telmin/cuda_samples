#ifndef COMMON_HPP
#define COMMON_HPP

#include <sys/time.h>

double get_ms(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((double)(tv.tv_sec)*1000 + (double)(tv.tv_usec)*0.001);
}

#endif
