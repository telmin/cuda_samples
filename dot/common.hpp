#ifndef COMMON_HPP
#define COMMON_HPP

#include <random>
#include <vector>
#include <sys/time.h>

template <typename T>
void initVector(std::vector<T>& vec, std::mt19937& rnd)
{
    std::uniform_real_distribution<float> dist(0.0f, 1.0f);

    for(auto& v : vec) {
	v = dist(rnd);
    }
}

double get_ms(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((double)(tv.tv_sec)*1000 + (double)(tv.tv_usec)*0.001);
}


#endif
