#include <iostream>
#include "common.hpp"
#include <random>
#include <cuda_runtime.h>

static double dotProduct_cpu(float& r, const std::vector<float>& a, const std::vector<float>& b, size_t num)
{
    double start = get_ms();

    float sum = 0.0f;
    for(size_t i = 0; i < num; ++i) {
	sum += a[i] * b[i];
    }
    r = sum;
    double end = get_ms();

    return (end - start);
}

__global__ void
dotProduct_cuda_atomic_kernel(float* r, float* a, float* b, unsigned int num)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if(idx < num) {
	float t = a[idx] * b[idx];
	atomicAdd(r, t);
    }
}

static double dotProduct_cuda_atomic(float& r, const std::vector<float>& a, const std::vector<float>& b, size_t num)
{
    float* da;
    float* db;
    float* dr;

    cudaError_t err;

    err = cudaMalloc((void**)&da, sizeof(float) * num);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to allocate device vector a %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMalloc((void**)&db, sizeof(float) * num);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to allocate device vector b %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMalloc((void**)&dr, sizeof(float));
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to allocate device r %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMemcpy(da, &a[0], sizeof(float) * num, cudaMemcpyHostToDevice);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to copy vector a %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMemcpy(db, &b[0], sizeof(float) * num, cudaMemcpyHostToDevice);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to copy vector a %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMemset(dr, 0, sizeof(float));

    int threadsPerBlock = 256;
    int blocksPerGrid = (num + threadsPerBlock - 1) / threadsPerBlock;

    unsigned int n = static_cast<unsigned int>(num);
    double start = get_ms();
    dotProduct_cuda_atomic_kernel<<<blocksPerGrid, threadsPerBlock>>>(dr, da, db, n);
    cudaDeviceSynchronize();
    double end = get_ms();

    cudaMemcpy(&r, dr, sizeof(float), cudaMemcpyDeviceToHost);

    cudaFree(dr);
    cudaFree(db);
    cudaFree(da);

    return (end - start);

}

template <int THREAD_NUM>
__global__ void
dotProduct_cuda_reduction_kernel(float* r, float* a, float* b, unsigned int num)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    __shared__ float tmpResult[THREAD_NUM];
    tmpResult[threadIdx.x] = 0;

    if(idx < num) {
	float t = a[idx] * b[idx];
	tmpResult[threadIdx.x] = t;
    }
    __syncthreads();

    for(unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
	if(threadIdx.x < s) {
	    tmpResult[threadIdx.x] += tmpResult[threadIdx.x + s];
	}
	__syncthreads();
    }

    if(threadIdx.x == 0) {
	atomicAdd(r, tmpResult[0]);
    }
}

static double dotProduct_cuda_reduction(float& r, const std::vector<float>& a, const std::vector<float>& b, size_t num)
{
    float* da;
    float* db;
    float* dr;

    cudaError_t err;

    err = cudaMalloc((void**)&da, sizeof(float) * num);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to allocate device vector a %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMalloc((void**)&db, sizeof(float) * num);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to allocate device vector b %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMalloc((void**)&dr, sizeof(float));
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to allocate device r %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMemcpy(da, &a[0], sizeof(float) * num, cudaMemcpyHostToDevice);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to copy vector a %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMemcpy(db, &b[0], sizeof(float) * num, cudaMemcpyHostToDevice);
    if(err != cudaSuccess) {
	fprintf(stderr, "failed to copy vector a %s\n", cudaGetErrorString(err));
	exit(EXIT_FAILURE);
    }

    err = cudaMemset(dr, 0, sizeof(float));

    constexpr int threadsPerBlock = 256;
    int blocksPerGrid = (num + threadsPerBlock - 1) / threadsPerBlock;

    unsigned int n = static_cast<unsigned int>(num);
    double start = get_ms();
    dotProduct_cuda_reduction_kernel<threadsPerBlock><<<blocksPerGrid, threadsPerBlock>>>(dr, da, db, n);
    cudaDeviceSynchronize();
    double end = get_ms();

    cudaMemcpy(&r, dr, sizeof(float), cudaMemcpyDeviceToHost);

    cudaFree(dr);
    cudaFree(db);
    cudaFree(da);

    return (end - start);

}

int main()
{
//    const size_t num = 1024000;
    const size_t num = 1000000;
    const size_t loop_num = 100;

    std::vector<float> a(num);
    std::vector<float> b(num);

    std::random_device rnd;
    std::mt19937 mt(rnd());

    initVector(a, mt);
    initVector(b, mt);

    double cpu_time = 0.0;
    float cpu_result;
    dotProduct_cpu(cpu_result, a, b, num);
    for(size_t i = 0; i < loop_num; ++i) {
	cpu_time += dotProduct_cpu(cpu_result, a, b, num);
    }
    cpu_time /= static_cast<double>(loop_num);

    double cuda_time = 0.0;
    float cuda_result;
    dotProduct_cuda_atomic(cuda_result, a, b, num);
    for(size_t i = 0; i < loop_num; ++i) {
	cuda_time += dotProduct_cuda_atomic(cuda_result, a, b, num);
    }
    cuda_time /= static_cast<double>(loop_num);

    double cuda_time2 = 0.0;
    float cuda_result2;
    dotProduct_cuda_reduction(cuda_result2, a, b, num);
    for(size_t i = 0; i < loop_num; ++i) {
	cuda_time2 += dotProduct_cuda_reduction(cuda_result2, a, b, num);
    }
    cuda_time2 /= static_cast<double>(loop_num);

    auto f = [](float l, float r) -> bool {
	if(fabs(l - r) < 1.0e-5) {
	    std::cout << "verify complete" << std::endl;
	    return true;
	} else {
	    std::cout << "verify failed " << l << " " << r << " " << fabs(l - r) << std::endl;
	    return false;
	}
    };

    f(cpu_result, cuda_result);
    f(cpu_result, cuda_result2);

    std::cout << "results " << cpu_result << " " << cuda_result << " " << cuda_result2 << std::endl;

    std::cout << "dotProduct cpu time : " << cpu_time << " msec" << std::endl;
    std::cout << "dotProduct gpu time : " << cuda_time << " msec" << std::endl;
    std::cout << "dotProduct gpu time2 : " << cuda_time2 << " msec" << std::endl;

    return 0;
}
